#!/usr/bin/env python3

# Import a prerequisite to allow gathering variables from script input
# Sys allows for standard input when calling the script
import sys
# Requests allows for gathering of API data from a URL
import requests
# JSON allows for processing of JSON-formatted data
import json
# CSV allows for writing out comma-separated files
import csv
# OS allows us to determine the working path of the script for the config file
import os
# ConfigParser allows us to have variables in a static file
import configparser
# Joblib allows multithreading
from joblib import Parallel, delayed

# The following packages will allow timezone offset determination from GPS coordinates
from datetime import datetime
from pytz import UnknownTimeZoneError
from pytz import timezone
from timezonefinder import TimezoneFinder

# Gather the input variables
DeviceTypes = sys.argv[1]
try:
    AccountGroup = sys.argv[2]
except IndexError:
    AccountGroup = ""

# Defining parameters here for easy modification
MaxReturn = "1000"
StartIndex = 0
IncludeVirtual = "false"

# Load the config file
try:
    configuration = configparser.ConfigParser()
    configuration.read(os.path.expanduser('~/.scriptconfig.txt'))
    ClientID = configuration.get("configuration", "ClientID")
    AuthToken = configuration.get("configuration", "AuthToken")
except KeyError:
    print("Unable to load the configuration file- please verify it exists!")
    quit()

# Define the standard URLs to be used later
HSOAPIUrl = 'https://api.aws.opennetworkexchange.net/api/v2/accounts'
HSOAccountsAPIUrl = ('https://api.aws.opennetworkexchange.net/api/v2/accounts/groups/{0}'.format(AccountGroup))  # Used if gathering from specific groups
DeviceAPIUrl = 'https://api.aws.opennetworkexchange.net/api/v2/devices'

# Define the output file
with open(DeviceTypes + '-' + AccountGroup + 'inventory.csv', mode='w') as inventory_csv:
    inventory_write = csv.writer(inventory_csv, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    # Write the beginning line defining the columns
    inventory_write.writerow(['DeviceID', 'DeviceType', 'HSOID', 'HSOName', 'CustID', 'UTCOffset', 'AccountGroups', 'Hostname', 'BrandID', 'Model', 'Serial', 'Monitoring type'])

# Define a starting list
HSOList = []

# Get the base information for the HSOs from the BAP API- has to be done in a loop as it will only return 1000 results at a time
# We'll base this on the start number, and just add a lot to cancel it later. This has the added benefit of adding a sanity catch
while StartIndex < 10000:

    # Define the API parameters here as it needs to be redefined each time- dependent on if an account group was provided
    if AccountGroup == "":
        HSOAPIParameters = {"access_token": AuthToken, "client_id": ClientID, "maxReturn": MaxReturn,
                            "includeGroups": True, "includeDevCounts": True, "startIndex": StartIndex}

        # Grab the API data and parse as JSON
        HSODataRaw = requests.get(HSOAPIUrl, params=HSOAPIParameters)
        HSOData = HSODataRaw.json()

        # If the accounts section isn't blank, add the results and increment by 1000, or break the loop
        if len(HSOData['accounts']) > 0:
            HSOList = HSOList + HSOData['accounts']
            StartIndex += 1000
        else:
            StartIndex = 10000

    else:
        HSOAPIParameters = {"access_token": AuthToken, "client_id": ClientID, "includeAccounts": True, "includeDevCounts": True}

        # Grab the API data and parse as JSON, then break the loop
        HSODataRaw = requests.get(HSOAccountsAPIUrl, params=HSOAPIParameters)
        HSOData = HSODataRaw.json()
        HSOList = HSOList + HSOData['accounts']
        StartIndex = 10000

print(f"Gathered {str(len(HSOList))} HSOs, moving on")


def TimezoneOffset(latitude, longitude, HSOOffset):
    try:
        TargetTime = str(int(datetime.now(timezone(TimezoneFinder().certain_timezone_at(lat=latitude, lng=longitude))).utcoffset().total_seconds() / 3600))  # Current time offset at target timezone
        return(TargetTime)
    except (AttributeError, UnknownTimeZoneError):  # If the lat/lon is 0, it will error and we can look at the reported offset
        if HSOOffset == int(datetime.now(tz=timezone("America/New_York")).utcoffset().total_seconds() * 1000):  # This is the default value, so list as unconfirmed
            return f"{int(HSOOffset / 3600000)}_Unconfirmed"
        else:
            return str(int(HSOOffset / 3600000))  # Otherwise, return the start based on the reported offset


def main(HSOInfo):
    # Start with the HSO
    if AccountGroup == "":
        HSOID = HSOInfo['id']
        # If we're globally getting HSOs, check if there are any monitored devices and aggregate group membership for summary
        if HSOInfo['totalMonitored'] is 0: return

    else:
        HSOID = HSOInfo

        # Getting the accounts via a group excludes all attributes, so we need to get these separately to check time zones
        HSOInfoRaw = requests.get(f'{HSOAPIUrl}/{HSOID}', params=HSOAPIParameters)
        HSOInfo = HSOInfoRaw.json()

    # Iterate through the groups to combine them into a single line
    GroupMembership = ""
    for GroupID in HSOInfo['accountGroups']:
        if GroupID['id'] == 179: return  # Check for blacklisted HSO groups
        GroupMembership = GroupMembership + "&" + str(GroupID['id'])

    # Get the offset for the timezone
    UTCOffset = TimezoneOffset(HSOInfo['latitude'], HSOInfo['longitude'], HSOInfo['timeZoneOffset'])

    # Define the API parameters
    DeviceAPIParameters = {"access_token": AuthToken, "client_id": ClientID, "maxReturn": MaxReturn, "types": DeviceTypes, "hsoid": HSOID, "includeVirtual": IncludeVirtual}

    # Grab the API data and parse as JSON
    DevicesDataRaw = requests.get(DeviceAPIUrl, params=DeviceAPIParameters)
    DevicesData = DevicesDataRaw.json()

    # Loop through the gathered devices
    for DeviceInfo in DevicesData['devices']:

        # Parse out the relevant info
        # Possible that there is no hostname.  If so, use the NASID
        if DeviceInfo['name'] == "":
            DeviceHostname = DeviceInfo['nasid']
        else:
            DeviceHostname = DeviceInfo['name']

        # Models and serials may have spaces- strip these out
        DeviceModel = DeviceInfo['model'].replace(",", "_")
        DeviceSerial = DeviceInfo['serial'].replace(",", "_")

        # Define the file
        with open(DeviceTypes + '-' + AccountGroup + 'inventory.csv', mode='a') as inventory_csv:
            inventory_write = csv.writer(inventory_csv, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

            # Write the beginning line defining the columns
            inventory_write.writerow([DeviceInfo['id'], DeviceInfo['type'], HSOID, HSOInfo['name'].replace(",", " "), HSOInfo['custid'], UTCOffset, GroupMembership, DeviceHostname.replace(",", " "), DeviceInfo['brandId'], DeviceModel, DeviceSerial, DeviceInfo['statusType']])


# Most of the processing is defined as a function to allow "return" to break the individual DIDs
Parallel(n_jobs=10, verbose=10)(delayed(main)(HSOInfo) for HSOInfo in HSOList)

print(f"Finished gathering devices for device types {str(DeviceTypes)}")
